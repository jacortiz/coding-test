export const LOGO_SIZE = 32;
export const BUTTON_SIZE = 55;
export const BUTTON_SPACING = 16;
export const CONTAINER_SPACING = 20;
export const SEARCH_SPACING = 15;

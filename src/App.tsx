import React, { Component } from "react";
import { createStyles, withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import * as _ from "lodash";

import { API_URL } from "./constants/Environment";
import HeaderNav from "./components/_shared/Header";
import Users from "./components/users/Users";
import { User } from "./interfaces/user.interface";
import { findUser } from "./lib/helpers";

interface OwnState {
  hasError: boolean;
  users: User[];
  initUsers: User[];
}
class App extends Component<any, OwnState> {
  public state = {
    hasError: false,
    users: [],
    initUsers: []
  };
  public componentDidMount() {
    this.loadUser();
  }

  public loadUser = () => {
    fetch(API_URL).then(result => {
      if (result.status !== 200) {
        this.setState({ hasError: true });
        return;
      } else {
        result.json().then(data => {
          this.sort(data.results, "date_of_birth", "desc");
        });
      }
    });
  };

  public sort = (users: User[], key: string, order: "desc" | "asc") => {
    const sortedData = _.orderBy(users, key, order);
    this.setState({
      users: sortedData,
      hasError: false,
      initUsers: sortedData
    });
  };

  public onTextChange = (event: any, users: User[]) => {
    const { initUsers } = this.state;
    const searchParams = event.target.value.toLocaleLowerCase().trim();
    const searchResult = findUser(users, searchParams);
    const newData = _.isEmpty(searchResult) ? initUsers : searchResult;
    this.setState({ users: newData });
    //hackish :( im planning to use context here ended up stating the state haha.
  };

  public render() {
    const { classes } = this.props;
    const { users, hasError } = this.state;
    return (
      <div className={classes.root}>
        <HeaderNav />
        <Container fixed>
          {!hasError && !_.isEmpty(users) ? (
            <Users
              data={users}
              refreshData={this.loadUser}
              sort={this.sort}
              onSearch={(event: any) => this.onTextChange(event, users)}
            />
          ) : (
            <p>Please wait/reload, no data found or forbidden (400 errors).</p>
          )}
        </Container>
      </div>
    );
  }
}

const useStyles = () =>
  createStyles({
    root: {
      flex: 1
    }
  });

export default withStyles(useStyles)(App);

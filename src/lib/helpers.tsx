import moment from "moment";
import * as _ from "lodash";
import { User } from "../interfaces/user.interface";

export const convertDateFromNow = (date: Date) => {
  return moment(date).fromNow();
};

export const findUser = (users: User[], searchParams: string) => {
  let searchData: User[] = [];

  if (!!searchParams) {
    searchData = _.filter(users, function(user: User) {
      const nameLike =
        user.name
          .toLocaleLowerCase()
          .trim()
          .indexOf(searchParams) !== -1;
      const addressLike =
        user.address
          .toLocaleLowerCase()
          .trim()
          .indexOf(searchParams) !== -1;
      const genderLike =
        user.gender
          .toLocaleLowerCase()
          .trim()
          .indexOf(searchParams) !== -1;
      const countryLike =
        user.country
          .toLocaleLowerCase()
          .trim()
          .indexOf(searchParams) !== -1;
      return nameLike || addressLike || genderLike || countryLike;
    });
  }
  return searchData;
};

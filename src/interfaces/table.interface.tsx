export interface TableProps {
  tableDef: {
    headers: {
      label: string;
      key: string;
      sortable?: boolean;
    }[];
    columns: string[];
    override?: any;
  };
  data: any[];
}

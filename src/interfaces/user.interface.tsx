export interface User {
  name: string;
  address: string;
  date_of_birth: Date;
  gender: string;
  country: string;
  [key: string]: string | Date;
}

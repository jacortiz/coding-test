import React, { Component } from "react";
import { createStyles, withStyles } from "@material-ui/core/styles";
import { User } from "../../interfaces/user.interface";
import CommonTable from "../_shared/CommonTable";
import { SearchText } from "../_shared/Search";
import { convertDateFromNow } from "../../lib/helpers";
import { CommonButton } from "../_shared/Button";
import {
  BUTTON_SIZE,
  CONTAINER_SPACING,
  BUTTON_SPACING,
  SEARCH_SPACING
} from "../../constants/GlobalStyles";

interface OwnProps {
  classes: any;
  data: User[];
  refreshData: () => void;
  sort: (data: User[], key: string, order: "desc" | "asc") => void;
  onSearch: (event: any, users: User[]) => void;
}
class Users extends Component<OwnProps> {
  public render() {
    const { classes, data } = this.props;
    const tableDef = {
      headers: [
        {
          label: "Name",
          key: "name",
          sortable: true
        },
        { label: "Address", key: "address" },
        {
          label: "Date of birth",
          key: "date_of_birth",
          sortable: true
        },
        { label: "Gender", key: "gender" },
        { label: "Country", key: "country" }
      ],
      columns: ["name", "address", "date_of_birth", "gender", "country"],
      override: {
        date_of_birth: (date: Date) => convertDateFromNow(date)
      }
    };

    return (
      <div className={classes.root}>
        <div className={classes.containerSpacing}>
          <CommonButton
            title="Refresh"
            onClick={() => this.props.refreshData()}
            classes={classes.buttonSpacing}
          />
          <SearchText
            label="Search"
            onChange={(event: any) => this.props.onSearch(event, data)}
            classes={classes.searchText}
          />
        </div>
        <CommonTable
          data={data}
          tableDef={tableDef}
          refreshData={this.props.refreshData}
          sort={this.props.sort}
        />
      </div>
    );
  }
}

const useStyles = () =>
  createStyles({
    root: {
      flex: 1
    },
    containerSpacing: {
      margin: CONTAINER_SPACING
    },
    buttonSpacing: {
      height: BUTTON_SIZE,
      marginTop: BUTTON_SPACING
    },
    searchText: {
      marginLeft: SEARCH_SPACING
    }
  });

export default withStyles(useStyles)(Users);

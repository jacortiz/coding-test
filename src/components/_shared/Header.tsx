import React, { Component } from "react";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";

import logo from "../../assets/images/logo.svg";
import { LOGO_SIZE } from "../../constants/GlobalStyles";

class HeaderNav extends Component<any> {
  public render() {
    const { classes } = this.props;
    return (
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <img src={logo} className={classes.logo} alt="logo" />
          <Typography variant="h6">Scout</Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

const useStyles = (theme: Theme) =>
  createStyles({
    menuButton: {
      marginRight: theme.spacing(2)
    },
    logo: {
      height: LOGO_SIZE
    }
  });

export default withStyles(useStyles)(HeaderNav);

import React, { Component } from "react";
import { createStyles, withStyles } from "@material-ui/core/styles";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableSortLabel
} from "@material-ui/core";
import { TableProps } from "../../interfaces/table.interface";
import { User } from "../../interfaces/user.interface";

interface OwnProps {
  classes: any;
  refreshData: () => void;
  sort: (data: User[], key: string, order: "desc" | "asc") => void;
}

interface OwnState {
  direction: "desc" | "asc";
}

class CommonTable extends Component<OwnProps & TableProps, OwnState> {
  public readonly state: OwnState = {
    direction: "desc"
  };
  public sortLabel = (key: string) => {
    const { data, sort } = this.props;
    const { direction } = this.state;
    const sortBy = direction === "asc" ? "desc" : "asc";
    return (
      <TableSortLabel
        active={true}
        direction={sortBy}
        onClick={() => {
          sort(data, key, sortBy);
          this.setState({ direction: sortBy });
        }}
      />
    );
  };
  public render() {
    const { data, tableDef } = this.props;

    return (
      <Table>
        <TableHead>
          <TableRow>
            {tableDef.headers.map(header => (
              <TableCell align="center" key={header.key}>
                {header.sortable && this.sortLabel(header.key)}
                {header.label}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((rowData, index) => {
            return (
              <TableRow key={index}>
                {tableDef.columns.map(value => {
                  return (
                    <TableCell align="center" scope="row" key={value}>
                      {rowData[value]
                        ? value in tableDef.override
                          ? tableDef.override[value](rowData[value])
                          : rowData[value]
                        : ""}
                    </TableCell>
                  );
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    );
  }
}

const useStyles = () =>
  createStyles({
    root: {
      flex: 1
    }
  });

export default withStyles(useStyles)(CommonTable);

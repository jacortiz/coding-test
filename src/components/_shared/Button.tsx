import React from "react";
import { Button } from "@material-ui/core";

interface ButtonProps {
  onClick: () => void;
  title: string;
  classes?: any;
}

export const CommonButton: React.SFC<ButtonProps> = ({
  onClick,
  title,
  classes
}) => (
  <Button className={classes} variant="contained" onClick={onClick}>
    {title}
  </Button>
);

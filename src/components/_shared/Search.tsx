import React from "react";
import { TextField } from "@material-ui/core";

interface SearchTextProps {
  onChange: (event: any) => void;
  label: string;
  classes?: any;
}

export const SearchText: React.SFC<SearchTextProps> = ({
  onChange,
  label,
  classes
}) => (
  <TextField
    onChange={onChange}
    id="filled-basic"
    label={label}
    className={classes}
    type="search"
    margin="normal"
    variant="filled"
  />
);
